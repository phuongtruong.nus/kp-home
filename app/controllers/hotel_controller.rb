class HotelController < ApplicationController
  def index
  end

  def rooms
  end

  def foods
  	@mains = Food.where(dish_type_id: "1");
	@desserts = Food.where(dish_type_id: "2");
	@drinks = Food.where(dish_type_id: "3");
  end
end
