class CreateFoods < ActiveRecord::Migration[5.0]
  def change
    create_table :foods do |t|
    	t.string :name 
    	t.float :price
    	t.string :images
    	t.references :dish_type, foreign_key: true
      t.timestamps
    end
  end
end
