class AddPurchasedAtToBooking < ActiveRecord::Migration[5.0]
  def change
  	add_column :bookings,:purchased_at, :date
  end
end
